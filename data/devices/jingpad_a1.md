---
name: "JingLing JingPad A1/C1"
deviceType: "tablet"
portType: "Halium 10.0"
buyLink: "https://en.jingos.com/jingpad-a1/"
portStatus:
  - categoryName: "Actors"
    features:
      - id: "manualBrightness"
        value: "+"
      - id: "notificationLed"
        value: "x"
      - id: "torchlight"
        value: "+"
      - id: "vibration"
        value: "+"
  - categoryName: "Camera"
    features:
      - id: "flashlight"
        value: "+"
      - id: "photo"
        value: "+"
      - id: "video"
        value: "+"
      - id: "switchCamera"
        value: "+"
  - categoryName: "Cellular"
    features:
      - id: "carrierInfo"
        value: "+"
      - id: "dataConnection"
        value: "+"
      - id: "dualSim"
        value: "x"
      - id: "calls"
        value: "x"
      - id: "mms"
        value: "x"
      - id: "pinUnlock"
        value: "+"
      - id: "sms"
        value: "x"
      - id: "audioRoutings"
        value: "x"
      - id: "voiceCall"
        value: "x"
  - categoryName: "Endurance"
    features:
      - id: "batteryLifetimeTest"
        value: "+-"
      - id: "noRebootTest"
        value: "+-"
  - categoryName: "GPU"
    features:
      - id: "uiBoot"
        value: "+"
      - id: "videoAcceleration"
        value: "+"
  - categoryName: "Misc"
    features:
      - id: "anboxPatches"
        value: "x"
        bugTracker: "https://gitlab.com/ubports/community-ports/pinephone/-/issues/84"
      - id: "apparmorPatches"
        value: "+"
      - id: "batteryPercentage"
        value: "-"
      - id: "offlineCharging"
        value: "+"
      - id: "onlineCharging"
        value: "+"
      - id: "recoveryImage"
        value: "+"
      - id: "factoryReset"
        value: "+"
      - id: "rtcTime"
        value: "+"
      - id: "sdCard"
        value: "+"
      - id: "shutdown"
        value: "+"
      - id: "wirelessCharging"
        value: "x"
      - id: "wirelessExternalMonitor"
        value: "-"
      - id: "waydroid"
        value: "+"
  - categoryName: "Network"
    features:
      - id: "bluetooth"
        value: "+"
      - id: "flightMode"
        value: "+"
      - id: "hotspot"
        value: "-"
      - id: "nfc"
        value: "x"
      - id: "wifi"
        value: "+-"
  - categoryName: "Sensors"
    features:
      - id: "autoBrightness"
        value: "x"
      - id: "fingerprint"
        value: "+"
      - id: "gps"
        value: "+-"
      - id: "proximity"
        value: "x"
      - id: "rotation"
        value: "+"
      - id: "touchscreen"
        value: "+"
  - categoryName: "Sound"
    features:
      - id: "earphones"
        value: "x"
      - id: "loudspeaker"
        value: "+"
      - id: "microphone"
        value: "+"
      - id: "volumeControl"
        value: "+"
  - categoryName: "USB"
    features:
      - id: "mtp"
        value: "x"
        bugTracker: "https://forums.ubports.com/topic/4189/mtp-on-mainline-devices-with-umtp-responder?_=1612121594185"
      - id: "adb"
        value: "x"
      - id: "wiredExternalMonitor"
        value: "x"
deviceInfo:
  - id: "cpu"
    value: "UNISOC Tiger T7510"
  - id: "chipset"
    value: "8-cores chipset with 4x Cortex-A75 cores clocked at 2.0GHz and 4x Cortex-A55 cores clocked at 1.8GHz"
  - id: "gpu"
    value: "PowerVR Rogue GM9446"
  - id: "rom"
    value: "256 GB"
  - id: "ram"
    value: "8 GB"
  - id: "battery"
    value: "8000 mAh"
  - id: "display"
    value: '11" Multi-Touch display'
  - id: "rearCamera"
    value: "Single 15.9MP″, LED Flash"
  - id: "frontCamera"
    value: "Single 8MP"
  - id: "arch"
    value: "arm64"
  - id: "dimensions"
    value: "243mm x 178mm x 11.2mm"
  - id: "weight"
    value: "'less than 500g'"
externalLinks:
  - name: "Subforum on UBports Forum"
    link: "https://forums.ubports.com/category/46/devices"
    icon: "yumi"
  - name: "Telegram chat"
    link: "https://t.me/UT_on_JingPad"
    icon: "telegram"
  - name: "Source repository"
    link: "https://gitlab.com/ubports/community-ports/android10/jingpad-a1"
    icon: "gitlab"
---
